[![pipeline status](https://gitlab.com/ChSamuel/tugas-kelompok-2-kd03/badges/master/pipeline.svg?style=flat-square)]
(https://gitlab.com/ChSamuel/tugas-kelompok-2-kd03/commits/master)
[![pipeline coverage](https://gitlab.com/ChSamuel/tugas-kelompok-2-kd03/badges/master/coverage.svg?style=flat-square)]

Nama anggota kelompok KD 03 :
Abdurrahman Luqmanul Hakim, NPM : 1806141113
Christopher Samuel, NPM : 1806141151
Mohamad Aziz Gafirazi Irfandi, NPM : 1806141315
Muhammad Raihan Armon, NPM : 1806141372

Link herokuapp : search-it-event2.herokuapp.com

Aplikasi ini bertujuan untuk menyebarkan informasi event tech/IT 
kepada para pengunjung site. Pengunjung akan menuliskan preference
tema event IT yang mau mereka kunjungi, lalu jadwal yang
ditampilkan akan disesuaikan dengan preference mereka. Selain itu,
ada halaman untuk menambahkan jadwal IT event lain, halaman About Us,
dan forum diskusi mengenai event IT.

Fitur yang akan diimplementasikan:
1. Menambah fitur login
2. Menambahkan ajax
3. Menambahkan JQuery dan Javascript