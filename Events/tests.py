from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest

from .models import Category
from .forms import CategoryForm
from .views import searchevents, filter_events

class EventsUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        # self.event = reverse(':events')
        # self.project = Feedback.objects.create(
        #     feedback='Great Website'
        # )
        # self.project1 = Feedback.objects.create(
        #     feedback='Great Website'
        # )


    def test_check_app(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_check_object(self):
        self.assertFalse(Category.objects.all().exists())
        category1 = Category.objects.create(name="gafi", age="19", category = 'general')
        self.assertTrue(isinstance(category1, Category))
        self.assertEqual(category1.name, "gafi")

    def test_check_form(self):
        form = CategoryForm()
        self.assertFalse(form.is_valid())

    def test_form_valid(self):

        form = CategoryForm(data = {
        'name': 'gafi', 'age': '19', 'category': 'general'
        })

        self.assertTrue(form.is_valid())

    # def test_no_data(self):
    #     form = CategoryForm(data={})

    #     self.assertFalse(form.is_valid())
    #     self.assertEquals(len(form.errors), 1)

    # def test_event(self):
    #     response = self.client.get(self.event, { "feedback": Category.objects.all()})
    #     self.assertEquals(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'events.html')

    def test_views_methods(self):
        found = resolve('/events')
        self.assertEqual(found.func, filter_events)
        found = resolve('/')
        self.assertEqual(found.func, searchevents)


    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'searchevents.html')
