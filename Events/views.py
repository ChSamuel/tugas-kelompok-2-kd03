from django.shortcuts import render, redirect
from RegisterEvent.models import Jadwal
from .models import Category
from .forms import CategoryForm
from django.http import JsonResponse


# Create your views here.
# def events(request):
#     return render(request,'events.html')

def searchevents(request):
    
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('filter_events')
    else:
        form = CategoryForm()
    
    
    return render(request, 'searchevents.html', {'form' : form})


def get_JSON_category(request,value):
    # result = []
    # for event in Jadwal.objects.all():
    #     if event.category == value:
    #         result.append(event)
    # print(result)
    # return JsonResponse({"data": list(Jadwal.objects.all())})
    if value == 'General':
        val = "general"
    elif value == 'Big Data':
        val = "bigdata"
    elif value == 'Software':
        val = "software"
    elif value == 'Machine Learning':
        val = "machinelearning"
    elif value == 'Web Development':
        val = "webdevelopment"
    result = list(Jadwal.objects.filter(category=val).values())
    return JsonResponse({"data": result})




def filter_events(request):
    events = Jadwal.objects.all()
    
    return render(request, 'events.html', {'events': events})

