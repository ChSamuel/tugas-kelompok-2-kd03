from django import forms
from django.forms import TextInput

from .models import Category

class CategoryForm(forms.ModelForm):
    
        # self.fields['age'].widget = TextInput(attrs={
        #     'id': 'fieldAge',
        #     'class': 'fieldz'
    class Meta:
        model = Category
        fields = ('category', 'name', 'age')
        labels = {
                'category'      : "Select Category",
                'name'          : "Your Name :",
                'age'           : "Age              :",
            }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        
        

        self.fields['name'].widget = TextInput(attrs={
            'id': 'fieldName',
            'class': 'fieldz',
        })

        self.fields['age'].widget = TextInput(attrs={
            'id': 'fieldAge',
            'class': 'fieldz',
        })

    

        