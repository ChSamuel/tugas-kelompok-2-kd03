from django.urls import path
from .views import *

urlpatterns = [
    path('events', filter_events, name='filter_events'),
    path('', searchevents, name = 'searchevents'),
    path('events/<str:value>' , get_JSON_category, name = "filtered_events")
]