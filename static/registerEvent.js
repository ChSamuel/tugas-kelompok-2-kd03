$(document).ready(function() {
    dummy = "getName"
    $.ajax({
        url: dummy,
        success: function(data){
            $("#dummyName").empty();
            $("#dummyName").append("Name : " + data.name);
            $("#dummyCategory").empty();
            $("#dummyCategory").append("Category : " + data.category);
            $("#dummyDate").empty();
            $("#dummyDate").append("Date : " + data.date);
            $("#dummyTime").empty();
            $("#dummyTime").append("Time : " + data.time);
            $("#dummyFee").empty();
            $("#dummyFee").append("Entrance Fee : " + data.entranceFee);
        },
    });

    $("#id_name").change(function(e){
        if($("#id_name").val() == "COMPFEST XI"){
            this.value='';
            alert("This event cannot be added more than once!");
        }
    })

    $("#changeFont").mouseover(function(event){
        var temp = document.getElementById("textanchor");
        if(temp.classList.contains('registerTextHelvetica')){
            document.getElementById("textanchor").classList.remove('registerTextHelvetica');
            document.getElementById("textanchor").classList.add('registerTextFranklin');
        }
        else{
            document.getElementById("textanchor").classList.remove('registerTextFranklin');
            document.getElementById("textanchor").classList.add('registerTextHelvetica');
        }
    })

    $("#changeFont").mouseout(function(event){
        var temp = document.getElementById("textanchor");
        if(temp.classList.contains('registerTextHelvetica')){
            document.getElementById("textanchor").classList.remove('registerTextHelvetica');
            document.getElementById("textanchor").classList.add('registerTextFranklin');
        }
        else{
            document.getElementById("textanchor").classList.remove('registerTextFranklin');
            document.getElementById("textanchor").classList.add('registerTextHelvetica');
        }
    })

    $("#changeFont").click(function(event){
        var temp = document.getElementById("textanchor");
        if(temp.classList.contains('registerTextHelvetica')){
            document.getElementById("textanchor").classList.remove('registerTextHelvetica');
            document.getElementById("textanchor").classList.add('registerTextFranklin');
        }
        else{
            document.getElementById("textanchor").classList.remove('registerTextFranklin');
            document.getElementById("textanchor").classList.add('registerTextHelvetica');
        }
    })
})