$(document).ready(function() {
$("#darkmode-switch").on("click", () => {
    if ($("#darkmode-switch").is(":checked")) {
        $("body").css("background-color", "black");

        // $(".text-bg-color").css("background-color", "black");
        $(".text-bg-color").css("color", "white");
        $("#day-night-txt").text("Night Mode");

        $("#navbar").css("background-color", "black");
        // $("#navbar-title").addClass("crimson-txt");
        // $(".navbar-text").css("color", "#E9F4FF");
        $(".accord-header").css("background-color", "black");
        $(".accord-bg").css("background-color", "black");

        // $(".accord-header").addClass("crimson-bg");
    } else {
        $("body").css("background-color", "white");

        // $(".text-bg-color").css("background-color", "white");
        $(".text-bg-color").css("color", "black");
        $("#day-night-txt").text("Day Mode");

        $("#navbar").css("background-color", "#84C4FF");
        // $("#navbar-title").removeClass("crimson-txt");
        // $(".navbar-text").css("color", "#333B43");
        $(".accord-header").css("background-color", "#84C4FF");
        $(".accord-bg").css("background-color", "white");

        // $(".accord-header").removeClass("crimson-bg");
    }
});

$( () => {
    $( "#accordion" ).accordion({active: false, collapsible: true});
});

})