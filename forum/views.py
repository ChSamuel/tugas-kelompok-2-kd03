from django.shortcuts import render
from .models import Commentsofwebsite
from . import forms
from .forms import Comment_Forum

# Create your views here.

def forum(request):
    temp = Commentsofwebsite.objects.all()
    if request.method == 'POST':
        form = forms.Comment_Forum(request.POST)
        if form.is_valid():
            form.save()
    else:    
        form = forms.Comment_Forum()
    return render(request, 'forum.html', {'form':form, 'temp': temp})
