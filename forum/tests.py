from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Commentsofwebsite
from .forms import Comment_Forum

# Create your tests here.
class AboutUsUnitTest(TestCase):
    def test_app_exists(self):
        response = Client().get('/Forum/')
        self.assertEqual(response.status_code, 200)

    def test_send_feedback(self):
        comment = Commentsofwebsite.objects.create(name = "Anon", message = "This is a comment")
        self.assertTrue(isinstance(comment, Commentsofwebsite))

    def test_validity(self):
        form = Comment_Forum()
        self.assertFalse(form.is_valid())

    def test_template(self):
        response = Client().get('/Forum/')
        self.assertTemplateUsed(response, 'forum.html')