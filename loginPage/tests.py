from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.http import HttpRequest
from .views import *

class LoginTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super(LoginTest, cls).setUpClass()
        cls.user = User.objects.create_user(
            'test', 'test@gmail.com', 'katasandi'
        )
    
    def test_login_exists(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code, 200)
    
    def test_login_post_fail(self):
        request = HttpRequest()
        request.method = 'POST'
        request.user = None
        request.POST['username'] = "Tester"
        request.POST['password'] = "katasandi"
        login(request)
    
    def test_logout(self):
        self.client.login(username='test', password='katasandi')
        response = self.client.get('/')
        self.assertTrue(response.status_code, 302)
    
    @classmethod
    def tearDownClass(cls):
        super(LoginTest, cls)