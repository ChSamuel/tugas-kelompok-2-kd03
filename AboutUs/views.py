from django.shortcuts import render, redirect
from .forms import Input_Form
# from .models import Feedback

# Create your views here.
def aboutus(request):
    response = {
        'form' : Input_Form,
    }

    return render(request,'about_us.html', response)

def submitted(request):
    return render(request, 'submitted.html')

def send(request):
    form = Input_Form(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # return redirect('about_us:aboutus')
            return redirect('about_us:submitted')

    response = {
        'form' : form
    }

    return render(request, 'about_us.html')
