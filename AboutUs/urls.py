from django.urls import path
from . import views

app_name = 'about_us'

urlpatterns = [
    path('', views.aboutus, name='aboutus'),
    path('send_feedback', views.send, name='send'),
    path('submitted', views.submitted, name='submitted')
]