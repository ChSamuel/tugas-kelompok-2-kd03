from django.test import TestCase, Client
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Feedback
from .forms import Input_Form
from . import views
from django.contrib.auth.models import User
from django.contrib.auth import login, logout

# Create your tests here.
class AboutUsUnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super(AboutUsUnitTest, cls).setUpClass()
        cls.user = User.objects.create_user(
            'username', 'email@email.com', 'password'
        )
        cls.user.first_name = 'firstname'
        cls.user.last_name = 'lastname'
        cls.user.save()

    def test_app_exists(self):
        response = Client().get('/AboutUs/')
        self.assertEqual(response.status_code, 200)

    def test_send_working(self):
        response = Client().get('/AboutUs/send_feedback')
        self.assertEqual(response.status_code, 200)

    def test_submitted_working(self):
        response = Client().get('/AboutUs/submitted')
        self.assertEqual(response.status_code, 200)

    def test_send_feedback(self):
        feedback = Feedback.objects.create(name = "Anon", message = "...(some feedback)...")
        self.assertTrue(isinstance(feedback, Feedback))

    def test_validity(self):
        form = Input_Form()
        self.assertFalse(form.is_valid())

    def test_template(self):
        response = Client().get('/AboutUs/')
        self.assertTemplateUsed(response, 'about_us.html')

    def test_aboutus_func(self):
        found = resolve('/AboutUs/')
        self.assertEqual(found.func, views.aboutus)

    def test_AboutUs_not_logged_in(self):
        response = self.client.get('/AboutUs/')
        html = response.content.decode()
        self.assertIn('Log in to give us feedback', html)
        self.assertNotIn('Please give us your feedback', html)

    def test_AboutUs_logged_in(self):
        self.client.login(username='username', password='password')
        response = self.client.get('/AboutUs/')
        html = response.content.decode()
        self.assertIn('Please give us your feedback', html)
        self.assertNotIn('Log in to give us feedback', html)

