from django.urls import path
from .views import *

urlpatterns = [
    path('', register, name='register'),
    path('success', success, name='success'),
    path('getName', getName, name='getName'),
]