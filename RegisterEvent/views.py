from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import Jadwal
from .forms import JadwalForm
from random import randrange

def register(request):
    if request.method == "POST":
        form = JadwalForm(request.POST)
        if(Jadwal.objects.filter(name=form['name']).count() == 0):
            if form.is_valid():
                form.save()
                return redirect("success")
    else:
        form = JadwalForm()
    
    return render(request, "register.html", {'form' : form})

def success(request):
    return render(request, "success.html")

def getName(request):
    randomObject = Jadwal.objects.get(name="COMPFEST XI")
    jsondata = {}
    jsondata['name'] = randomObject.name
    jsondata['category'] = randomObject.category.capitalize()
    jsondata['date'] = randomObject.date
    jsondata['time'] = randomObject.time
    jsondata['entranceFee'] = randomObject.entranceFee

    return JsonResponse(jsondata)